Page({
  data: {
    imageSrc: "",
  },
  downloadImage() {
    tt.downloadFile({
      url: "https://s3.pstatp.com/toutiao/static/img/logo.201f80d.png",
      header: {
        "content-type": "application/json",
      },
      success: (res) => {
        console.log("downloadFile success", res);
        tt.showToast({
          title: res.errMsg,
          icon: "success",
        });
        this.setData({
          imageSrc: res.tempFilePath,
        });
      },
      fail: (res) => {
        console.log("downloadFile fail", res);
        tt.showToast({
          title: res.errMsg,
          icon: "none",
        });
      },
      complete: (res) => {
        console.log("downloadFile complete", res);
      },
    });
  },
});